<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'App\Entity\Task' shared autowired service.

include_once $this->targetDirs[3].'/src/Entity/Task.php';

return $this->privates['App\\Entity\\Task'] = new \App\Entity\Task();
