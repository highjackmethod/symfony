<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'App\Domain\Mail\Manager\MailManager' shared autowired service.

include_once $this->targetDirs[3].'/src/Domain/Mail/Manager/MailManagerInterface.php';
include_once $this->targetDirs[3].'/src/Domain/Mail/Manager/MailManager.php';
include_once $this->targetDirs[3].'/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/CharsetObserver.php';
include_once $this->targetDirs[3].'/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/EncodingObserver.php';
include_once $this->targetDirs[3].'/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMimeEntity.php';
include_once $this->targetDirs[3].'/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/MimePart.php';
include_once $this->targetDirs[3].'/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mime/SimpleMessage.php';
include_once $this->targetDirs[3].'/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Message.php';

return $this->privates['App\\Domain\\Mail\\Manager\\MailManager'] = new \App\Domain\Mail\Manager\MailManager(($this->privates['Swift_Mailer'] ?? $this->load('getSwiftMailerService.php')), ($this->privates['Swift_Message'] ?? ($this->privates['Swift_Message'] = new \Swift_Message())));
