<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelTestDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/' => [[['_route' => 'form_index', '_controller' => 'App\\Controller\\TaskController::index'], null, null, null, false, false, null]],
            '/success' => [[['_route' => 'form_success', '_controller' => 'App\\Controller\\TaskController::success'], null, null, null, false, false, null]],
        ];
    }
}
