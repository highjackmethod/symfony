<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Task
 *
 * @package App\Entity
 */
class Task
{
    /**
     * @Assert\NotBlank
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     */
    public $email;

    /**
     * @Assert\NotBlank
     */
    public $category;
}