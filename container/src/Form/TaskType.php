<?php

namespace App\Form;

use App\Domain\Joke\Manager\JokeManagerInterface;
use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TaskType
 *
 * @package App\Form
 */
class TaskType extends AbstractType
{
    /** @var JokeManagerInterface */
    private $jokeManager;

    /**
     * TaskType constructor.
     *
     * @param JokeManagerInterface $jokeManager
     */
    public function __construct(JokeManagerInterface $jokeManager)
    {
        $this->jokeManager = $jokeManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $categories = $this->jokeManager->getCategories();

        $builder
            ->add('email', TextType::class)
            ->add('category', ChoiceType::class, ['choices' => $categories])
            ->add('save', SubmitType::class, ['label' => 'Send joke!'])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class
        ]);
    }
}