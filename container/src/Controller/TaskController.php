<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Domain\Joke\Manager\JokeManagerInterface;
use App\Domain\Mail\Manager\MailManagerInterface;
use App\Domain\File\Manager\FileManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskController
 *
 * @package App\Controller
 */
class TaskController extends AbstractController
{
    /** @var JokeManagerInterface */
    private $jokeManager;

    /** @var MailManagerInterface */
    private $mailManager;

    /** @var FileManagerInterface */
    private $fileManager;

    /** @var Task */
    private $task;

    /**
     * TaskController constructor.
     *
     * @param JokeManagerInterface $jokeManager
     * @param MailManagerInterface $mailManager
     * @param FileManagerInterface $fileManager
     * @param Task                 $task
     */
    public function __construct(
        JokeManagerInterface $jokeManager,
        MailManagerInterface $mailManager,
        FileManagerInterface $fileManager,
        Task $task
    ) {
        $this->jokeManager = $jokeManager;
        $this->mailManager = $mailManager;
        $this->fileManager = $fileManager;
        $this->task = $task;
    }

    /**
     * @Route("/", name="form_index")
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function index(Request $request)
    {
        $form = $this->createForm(TaskType::class, $this->task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('session')->set('data', $form->getData());
            return $this->redirectToRoute('form_success');
        }

        return $this->render('form/init.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/success", name="form_success")
     *
     * @return RedirectResponse|Response
     */
    public function success()
    {
        $session = $this->container->get('session');
        $data = $session->get('data');

        if (empty($data->category) || empty($data->email)) {
            return $this->redirectToRoute('form_index');
        }

        $joke = $this->jokeManager->getRandomJoke($data->category);
        $this->mailManager->send($data->email, $joke);
        $this->fileManager->writeToFile($joke);

        $session->clear();
        return $this->render('form/success.html.twig');
    }
}