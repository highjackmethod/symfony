<?php

namespace App\Domain\File\Manager;

/**
 * Interface FileManagerInterface
 *
 * @package App\Domain\File\Manager
 */
interface FileManagerInterface
{
    /**
     * @param string $joke
     */
    public function writeToFile(string $joke): void;
}