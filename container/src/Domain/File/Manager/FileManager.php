<?php

namespace App\Domain\File\Manager;

use App\Domain\File\Exception\FileOperationException;
use Exception;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FileManager
 *
 * @package App\Domain\File\Manager
 */
class FileManager implements FileManagerInterface
{
    /** @var string */
    private const PATH = '/tmp/1.txt';

    /** @var Filesystem */
    private $filesystem;

    /**
     * FileManager constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param string $joke
     *
     * @throws FileOperationException
     */
    public function writeToFile(string $joke): void
    {
        try {

            $this->filesystem->appendToFile(self::PATH, $joke . '\n ');

        } catch (Exception $e) {
            throw new FileOperationException('Something wrong with filesystem operations.');
        }
    }
}