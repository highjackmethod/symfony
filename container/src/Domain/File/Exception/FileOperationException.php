<?php

namespace App\Domain\File\Exception;

use Exception;

/**
 * Class FileOperationException
 *
 * @package App\Domain\File\Exception
 */
class FileOperationException extends Exception
{

}