<?php

namespace App\Domain\Joke\Manager;

/**
 * Interface JokeManagerInterface
 *
 * @package App\Domain\Joke\Manager
 */
interface JokeManagerInterface
{
    /**
     * @return array
     */
    public function getCategories(): array;

    /**
     * @param string $category
     *
     * @return string
     */
    public function getRandomJoke(string $category): string;
}