<?php

namespace App\Domain\Joke\Manager;

use Exception;
use GuzzleHttp\Client;
use App\Domain\Joke\Exception\ExternalApiException;

/**
 * Class JokeManager
 *
 * @package App\Domain\Joke\Manager
 */
class JokeManager implements JokeManagerInterface
{
    /** @var string */
    private const MAIN_URL = 'http://api.icndb.com';

    /** @var string */
    private const CATEGORIES_URL = '/categories';

    /** @var string */
    private const RANDOM_JOKE_URL = '/jokes/random?limitTo=';

    /** @var Client */
    private $client;

    /**
     * JokeManager constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     * @throws ExternalApiException
     */
    public function getCategories(): array
    {
        try {

            $categories = [];

            $response = $this->client->get(self::MAIN_URL . self::CATEGORIES_URL);
            $categoriesTmp = json_decode($response->getBody(), true)['value'];

            foreach ($categoriesTmp as $category) {
                $categories[ucfirst($category)] = $category;
            }

            return $categories;

        } catch (Exception $e) {
            throw new ExternalApiException('Something wrong with external API operations.');
        }
    }

    /**
     * @param string $category
     *
     * @return string
     * @throws ExternalApiException
     */
    public function getRandomJoke(string $category): string
    {
        try {

            $response = $this->client->get(self::MAIN_URL . self::RANDOM_JOKE_URL . "[$category]");
            $joke = json_decode($response->getBody(), true)['value']['joke'];

            return $joke;

        } catch (Exception $e) {
            throw new ExternalApiException('Something wrong with external API operations.');
        }
    }
}