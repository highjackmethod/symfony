<?php

namespace App\Domain\Joke\Exception;

use Exception;

/**
 * Class ExternalApiException
 *
 * @package App\Domain\Joke\Exception
 */
class ExternalApiException extends Exception
{

}