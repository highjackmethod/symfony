<?php

namespace App\Domain\Mail\Manager;

/**
 * Interface MailManagerInterface
 *
 * @package App\Domain\Mail\Manager
 */
interface MailManagerInterface
{
    /**
     * @param string $email
     * @param string $joke
     */
    public function send(string $email, string $joke): void;
}