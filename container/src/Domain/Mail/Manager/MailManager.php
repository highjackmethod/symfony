<?php

namespace App\Domain\Mail\Manager;

use App\Domain\Mail\Exception\MailSendException;
use Exception;
use Swift_Mailer;
use Swift_Message;

/**
 * Class MailManager
 *
 * @package App\Domain\Mail\Manager
 */
class MailManager implements MailManagerInterface
{
    /** @var Swift_Mailer */
    private $mailer;

    /** @var Swift_Message */
    private $message;

    /**
     * MailManager constructor.
     *
     * @param Swift_Mailer  $mailer
     * @param Swift_Message $message
     */
    public function __construct(Swift_Mailer $mailer, Swift_Message $message)
    {
        $this->mailer = $mailer;
        $this->message = $message;
    }

    /**
     * @param string $email
     * @param string $joke
     *
     * @throws MailSendException
     */
    public function send(string $email, string $joke): void
    {
        try {

            $outMessage = $this->message->setSubject('Some joke!')
                ->setFrom('info@example.com')
                ->setTo($email)
                ->setBody($joke, 'text/html');

            $this->mailer->send($outMessage);

        } catch (Exception $e) {
            throw new MailSendException('Something wrong with mail operations.');
        }
    }
}