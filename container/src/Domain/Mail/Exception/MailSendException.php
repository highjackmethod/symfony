<?php

namespace App\Domain\Mail\Exception;

use Exception;

/**
 * Class MailSendException
 *
 * @package App\Domain\Mail\Exception
 */
class MailSendException extends Exception
{

}