<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class TaskControllerTest
 *
 * @package App\Tests\Functional\Controller
 */
class TaskControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Send joke!')->form();

        $form['task[email]'] = 'some_email@site.ru';
        $form['task[category]'] = 'nerdy';

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect());

        $client->followRedirect();

        $this->assertContains(
            'Joke successfully sent and registered in the file!',
            $client->getResponse()->getContent()
        );
    }
}